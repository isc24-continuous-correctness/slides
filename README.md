# ISC'24 Tutorial: Continuous Correctness Checking for HPC Applications
This is a collection of material for the ISC'24 tutorial "Continuous Correctness Checking for HPC Applications" given by Adrian Schmitz, Alexander Hück, Joachim Jenke, Simon Schwitanski.

## Files
- [Slide Set for the Tutorial](https://git-ce.rwth-aachen.de/isc24-continuous-correctness/slides/-/raw/main/Continuous%20Correctness%20Checking%20for%20HPC%20Applications.pdf?ref_type=heads)

## Tool References
- Sanitizers: https://github.com/google/sanitizers
- Archer: https://github.com/llvm/llvm-project/tree/main/openmp/tools/archer
- MUST: https://itc.rwth-aachen.de/must
- TypeART: https://github.com/tudasc/TypeART
- CI Driver: https://git.rwth-aachen.de/adrianschmitz2/aixcilenz-ci-driver

## Preparation (Optional)
As a preparation for the hands-on part of tutorial, we would kindly ask you create an account at https://git-ce.rwth-aachen.de/. 
You can either 
1. Create an account by linking an existing GitHub account (preferred)
2. Use DFN-AAI Single Sign-On (only available for German research institutions).

If neither of the options is available to you, we can create an account for you. Please let us know via contact@hpc.rwth-aachen.de beforehand or personally during the tutorial.

## Contact Us
In case you have any questions beforehand or afterwards, do not hesitate to contact us via contact@hpc.rwth-aachen.de.